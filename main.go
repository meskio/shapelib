package main

import (
	"fmt"
)

func main() {
	ss := ShapeShifter{
		Cert:      "gZlp9k6br2vn2MLnmjXpNh8D/CrR5wtackwZof/iICv1QzPwkkTUa+7JVOKtPiD0NFwCMw",
		Target:    "37.218.247.60:23042",
		SocksAddr: "127.0.0.1:4430",
	}
	err := ss.Open()
	if err != nil {
		fmt.Printf("%v", err)
	}
	defer ss.Close()
	select {}
}
